const {addToCollectionValidation, getDataOfCollectionValidation} = require('./validations');

const express = require('express'),
    mongoose = require('mongoose'),
    {authGuard, jsonParser} = require("../common")


const app = express.Router();
app.use(jsonParser);
app.use(authGuard);

app.post('/add/:collection_name', addToCollectionValidation, async (req, res, next) => {
    const {body, Model} = req;
    const doc = new Model(body);
    await doc.save();
    res.json(doc);
})


app.get('/list/:collection_name', getDataOfCollectionValidation, async (req, res, next) => {
    const {Model} = req;
    const list = await Model.find();
    res.json(list);
})


module.exports = app;