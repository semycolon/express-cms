const {veh} = require('../common');
const {CustomType: CustomTypeClass} = require('../../lib/services/ContentTypeBuilder/ContentTypeClass')
const mongoose = require('mongoose');
const {EVM} = require("../../lib/misc/ValidationMessages");
const {check, param, body} = require('express-validator');
const debug = require('debug')('cms:validation')

module.exports = {
    addToCollectionValidation: [
        param('collection_name')
            .exists()
            .custom(checkIfCollectionExists)
        ,
        veh,
    ],

    getDataOfCollectionValidation: [
        param('collection_name')
            .exists()
            .custom(checkIfCollectionExists)
        ,
        veh,
    ],
}


function checkIfCollectionExists(name, {req}) {
    return new Promise(async (resolve, reject) => {
        try {
            const {CustomModelsService} = require("../../lib/services/ContentTypeBuilder/ContentModelService")
            req.Model = await CustomModelsService.getCustomModel(name);
            resolve();
        } catch (e) {
            debug(e);
            reject(EVM.not_found);
        }
    })
}