const {veh} = require("../common");
const mongoose = require('mongoose');
const {EVM} = require("../../lib/misc/ValidationMessages");
const UserModel = mongoose.model('User');
const {check, param, body} = require('express-validator');
const {CustomType: CustomTypeClass} = require('../../lib/services/ContentTypeBuilder/ContentTypeClass')
const debug = require('debug')('custom-type:validations')

module.exports = {
    createCustomTypeValidation: [
        check('name').custom(checkIfAvailable),
        check('label')
            .exists().withMessage(EVM.empty)
            .notEmpty().withMessage(EVM.empty)
            .isString().withMessage(EVM.text)
        ,
        check('name')
            .exists().withMessage(EVM.empty)
            .notEmpty().withMessage(EVM.empty)
            .isString().withMessage(EVM.text)
        ,
        veh,
    ],
    addFieldCustomTypeValidation: [
        param('name').custom(checkIfCustomModelExists)
        ,
        body('name')
            .exists().withMessage(EVM.empty)
            .notEmpty().withMessage(EVM.empty)
            .isString().withMessage(EVM.text)
            .bail()
            .custom(isFieldAvailable)
        ,
        body('label')
            .exists().withMessage(EVM.empty)
            .notEmpty().withMessage(EVM.empty)
            .isString().withMessage(EVM.text)
        ,
        body(['min', 'max']).optional().isInt().withMessage(EVM.number),
        body('unique').optional().isBoolean().withMessage(EVM.boolean),
        body('private').optional().isBoolean().withMessage(EVM.boolean),
        body('required').optional().isBoolean().withMessage(EVM.boolean),

        veh,
    ],
    deleteFieldCustomTypeValidation: [
        param('name').custom(checkIfCustomModelExists),
        param('custom_field_name').custom(checkIfFieldOfCustomModelExists),
        veh,
    ],
    detailedCustomTypeValidation: [
        param('name').custom(checkIfCustomModelExists),
        veh,
    ],
    deleteCustomTypeValidation:[
        param('name').custom(checkIfCustomModelExists),
        veh,
    ]
}


function checkIfCustomModelExists(name, {req}) {
    return new Promise(async (resolve, reject) => {
        try {
            req.custom_type = await CustomTypeClass.getInstance(name)
            resolve();
        } catch (e) {
            debug(e);
            reject(EVM.not_found);
        }
    })
}

function checkIfAvailable(name) {
    return new Promise(async (resolve, reject) => {
        try {
            await CustomTypeClass.getInstance(name)
            reject(EVM.already_exists);
        } catch (e) {
            resolve();
        }
    })
}

function isFieldAvailable(value, {req}) {
    return new Promise(((resolve, reject) => {
        const {custom_type} = req;
        if (!custom_type) {
            debug('ridi', 'custom_type should be available in request.')
            reject(EVM.error_500);
        } else {
            const field = custom_type.getDocument().fields.find(it => it.name === value);
            if (field) {
                reject(EVM.already_exists);
            } else {
                resolve();
            }
        }
    }))
}

function checkIfFieldOfCustomModelExists(value, {req}) {
    return new Promise(((resolve, reject) => {
        const {custom_type} = req;
        if (!custom_type) {
            // no need to validate
            reject(EVM.not_found);
        } else {
            const field = custom_type.getDocument().fields.find(it => it.name === value);
            if (!field) {
                reject(EVM.not_found);
            } else {
                req.custom_field_obj = field;
                resolve();
            }
        }
    }))
}
