const express = require('express'),
    {authGuard, jsonParser} = require("../common"),
    mongoose = require('mongoose'),
    validations = require("./validation"),
    CustomType = mongoose.model('ContentType'),
    {CustomType: CustomTypeClass} = require('../../lib/services/ContentTypeBuilder/ContentTypeClass');

const app = express.Router();
app.use(jsonParser);
app.use(authGuard);

app.get('/', async (req, res) => {
    res.json(await CustomType.find())
})

app.post('/:name', validations.addFieldCustomTypeValidation, async (req, res, next) => {
    const {custom_type, body} = req;
    await custom_type.addFields(body);
    res.send(custom_type.getDocument());
})

app.delete('/:name/:custom_field_name', validations.deleteFieldCustomTypeValidation, async (req, res, next) => {
    const {custom_type, custom_field_obj,} = req;
    await custom_type.deleteFields(custom_field_obj.name);
    res.send(custom_type.getDocument());
})


app.post('/', validations.createCustomTypeValidation, async (req, res, next) => {
    const {name, label} = req.body;
    await CustomTypeClass.createNew({name, label});
    const list = await CustomType.find()
    res.json(list);
})

app.get('/detailed/:name', validations.detailedCustomTypeValidation, async (req, res, next) => {
    res.json(req.custom_type.getDocument());
})

app.delete('/:name', validations.deleteCustomTypeValidation, async (req, res, next) => {
    try {
        await req.custom_type.delete()
        const list = await CustomType.find()
        res.json(list);
    } catch (e) {
        console.error(e);
        res.status(500).json({msg:'something went wrong.'})
    }
})


module.exports = app;