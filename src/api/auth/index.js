const express = require('express');
const {jsonParser} = require("../common");
const {loginValidators, registerValidators} = require("./validations");
const app = express.Router();
const mongoose = require('mongoose');
const {authGuard} = require("../common");
const UserModel = mongoose.model('User');
const debug = require('debug')('api:auth')

app.use(jsonParser);

if (!process.env.PRODUCTION) {
    const delay = (req,res,next) => {
        setTimeout(() => {
            next();
        },2000);
    }
   // app.use(delay);
}

app.post('/register', registerValidators, async (req, res, next) => {
    const user = new UserModel(req.body);
    await user.save()
    const token = await user.generateJWT()
    return res.json({user, token});
});

app.post('/login', loginValidators, async (req, res, next) => {
    const {username, password} = req.body;
    const user = await UserModel.findOne({username})
    if (user) {
        try {
            const match = await user.comparePassword(password);
            if (match) {
                const token = await user.generateJWT()
                return res.json({user, token});
            }
        } catch (e) {
            debug(e);
        }
        return res.status(422).json({errors:[{msg: 'username and password does not match',param:'password'}]})
    } else {
        return res.status(422).json({errors:[{msg:'not found!',param:'username'}]})
    }
})

app.get('/me', authGuard, (req, res, next) => {
    return res.json({user: req.user});
})

module.exports = app;