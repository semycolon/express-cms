const {veh} = require("../common");
const mongoose = require('mongoose');
const UserModel = mongoose.model('User');
const {check, validationResult} = require('express-validator');

module.exports = {
    registerValidators: [
        check('username')
            .exists().withMessage('should not be empty')
            .notEmpty().withMessage('should not be empty').bail()
            .isLength({min: 4}).withMessage('too short').bail()
            .isLength({max: 36}).withMessage('too long')
            .custom(isUsernameAvailable).bail()
            .custom(superAdminRegisterValidation),

        check('password')
            .exists().withMessage('should not be empty')
            .notEmpty().withMessage('should not be empty')
            .bail()
            .isLength({min: 6}).withMessage('too short')
            .isLength({max: 36}).withMessage('too long'),


        veh,
    ],

    loginValidators: [
        check('username')
            .exists().withMessage('should not be empty')
            .notEmpty().withMessage('should not be empty')
            .bail()
            .custom(usernameExists),

        check('password')
            .exists().withMessage('should not be empty')
            .notEmpty().withMessage('should not be empty')
        ,

        veh,
    ]
}

function usernameExists(username) {
    return UserModel.findOne({username}).then(user => {
        if (!user) {
            return Promise.reject('no such user');
        }
    })
}

function isUsernameAvailable(username) {
    return UserModel.findOne({username}).then(user => {
        if (user) {
            return Promise.reject('username already taken');
        }
    })
}

function superAdminRegisterValidation(username, {req}) {
    if (req.body.superAdmin) {
        return UserModel.findOne({superAdmin: true}).then(user => {
            if (user) {
                return Promise.reject('super admin user already exists!');
            }
        });
    }
    return Promise.resolve();
}
