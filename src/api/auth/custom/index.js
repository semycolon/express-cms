const express = require('express');
const router = express.Router();


router.get('/child', (req, res, next) => {
    res.send('child custom auth module');
})

router.get('/*', (req, res, next) => {
    res.send('custom auth module');
})

module.exports = router;