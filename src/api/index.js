const express = require('express');
const router = express.Router()
const fs = require('fs');
const {join} = require('path')
let cache;

function loadApis(targetDir, prefix) {
    const directories = fs.readdirSync(targetDir).filter(it => {
        const path = join(targetDir, it);
        return fs.existsSync(path) && fs.lstatSync(path).isDirectory();
    })
    for (let dir of directories) {
        const path = join(targetDir, dir);

        let url = `${prefix}/${dir}`;
        loadApis(path, url);

        let indexPath = join(path, 'index.js');
        if (fs.existsSync(indexPath)) {
            router.use(url, require(indexPath));
        }
    }

}

module.exports.getApiRoutes = async function () {
    if (!cache) {
        loadApis(__dirname, '');
        router.get('/',(req,res) =>{
            res.send('api is working :), this is the base url path')
        })
        cache = router;
    }
    return cache;
}