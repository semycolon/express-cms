const bodyParser = require('body-parser');
const {validationResult, header} = require('express-validator');
const mongoose = require('mongoose');

module.exports = {
    veh,
    jsonParser: bodyParser.json(),
    urlencodedParser: bodyParser.urlencoded({extended: false}),
    authGuard: [
        header('token').exists().withMessage('header token must be sent.')
            .custom(isTokenValid),
        veh,
    ]
}

function veh(req, res, next) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({errors: errors.array()});
    }
    return next();
}

function isTokenValid(token, {req}) {
    return new Promise(async (resolve, reject) => {
        try {
            if (token) {
                const UserModel = mongoose.model('User');
                const user = await UserModel.verifyJwtToken(token);
                if (user) {
                    req.user = user;
                    return resolve()
                } else {
                    return reject('invalid token')
                }
            }
        } catch (e) {
            console.error(e);
        }
        return reject('invalid token')
    })
}