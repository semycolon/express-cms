const express = require('express');
const {getApiRoutes} = require("./api");
const app = express();
const cors = require('cors');

app.use(cors());

module.exports.generateApp = async function (baseUrl) {
    const apis = await getApiRoutes();
    app.use(baseUrl,apis);
    return app;
};