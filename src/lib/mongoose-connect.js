const {join} = require('path');
const fs = require('fs');
const mongoose = require('mongoose');
const debug = require('debug')('mongoose');
const error = require('debug')('mongoose:error');

module.exports.initDatabase = async function (custom_db_name) {
    setDeprecatedStuff()
    const success = await connectToDatabase(custom_db_name);
    if (success) {
        initModels()
    }
    return success;
}

function errorHandler(err) {
    error(err);
}

function onConnected() {
    debug('connected to database.')
}

function onDisconnected() {
    debug('database disconnected.')
}

async function connectToDatabase(custom_db_name) {
    try {
        const connStr = custom_db_name || process.env.MONGODB_CONNECTION_STRING
        if (!connStr) {
            throw new Error('process.env.MONGODB_CONNECTION_STRING is not available.')
        }
        debug('connecting to database. make sure it\'s listening...')
        const newMongoose = await mongoose.connect(connStr, {config: {autoIndex: false}});
        mongoose.connection.on('error', errorHandler);
        mongoose.connection.on('connected', onConnected);
        mongoose.connection.on('disconnected', onDisconnected);
        return Boolean(newMongoose);
    } catch (e) {
        errorHandler(e);
    }
    error('connection failed.')
    return false;
}

function initModels() {
    const path = join(__dirname, './models');
    const list = fs.readdirSync(path).filter(it => it.endsWith('.js'));
    for (let file of list) {
        require(join(path, file));
    }
}

function setDeprecatedStuff() {
    mongoose.set('useNewUrlParser', true);
    mongoose.set('useFindAndModify', false);
    mongoose.set('useCreateIndex', true);
    mongoose.set('useUnifiedTopology', true);
}
