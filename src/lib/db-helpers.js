const mongoose = require('mongoose');

module.exports = {
    dropCollection(collection_name) {
        return new Promise((resolve, reject) => {
            const {db} = mongoose.connection;
            db.listCollections({name: collection_name})
                .next(function (err, collinfo) {
                    if (err){
                        return reject(err);
                    }
                    if (collinfo) {
                        // The collection exists
                        db.dropCollection(collection_name, function (err, result) {
                            if (err) {
                                return reject(err);
                            } else {
                                return resolve(result);
                            }
                        });
                    }

                    return resolve();
                });


        })
    }
}