let cache = {};
const mongoose = require('mongoose');
const ContentTypeModel = mongoose.model('ContentType')
const debug = require('debug')('CustomModelService')

function deleteModelIfAlreadyRegistered(collectionName) {
    if (mongoose.connection.models[collectionName]) {
        delete mongoose.connection.models[collectionName];
    }
}

function registerModel(ct) {
    let obj = {};
    for (let it of ct.fields) {
        const {name,type_name} = it;
        const type = mongoose.SchemaTypes[type_name]
        obj[name] = {...it, type,};
    }
    const schema = new mongoose.Schema(obj, {autoIndex: obj.autoIndex,});
    const collectionName = ct.name;

    deleteModelIfAlreadyRegistered(collectionName);
    const model = mongoose.model(collectionName, schema, collectionName);
    cache[collectionName] = model;
    return model;
}

module.exports.CustomModelsService = {
    async registerAllModels() {
        cache = {};
        const list = await ContentTypeModel.find({})
        for (const ct of list) {
            registerModel(ct)
        }
    },
    async getCustomModel(collection_name) {
        if (cache[collection_name]) {
            return cache[collection_name]
        }

        let ct = await ContentTypeModel.findOne({name: collection_name});
        if (ct) {
            return registerModel(ct);
        }
        throw new Error('no such collection');
    },
    getAllModels() {
        return cache;
    },
    async emptyAllModels(registerAllModels = true) {
        if (registerAllModels) {
            await this.registerAllModels();
        }
        for (let k in cache) {
            const Model = cache[k];
            debug(`deleting docs for model:${k}`);
            await Model.deleteMany({});
        }
    },

}
