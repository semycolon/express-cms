module.exports = {
    getField(opt) {
        return {
            type_name: 'String',
            name: opt.name,
            label: opt.label,
            default: opt.default || "",
            required: opt.required || null,
            private: opt.private || null,
            unique: opt.unique || null,
            index:opt.index || null,
            sparse:opt.sparse || null,
            //string specific options
            lowercase:opt.lowercase || null,
            uppercase:opt.uppercase || null,
            trim:opt.trim || null,
            // match:opt.match || null,
          //  enum:opt.enum || null,
          //   minlength:opt.minlength || null,
          //   maxlength:opt.maxlength || null,
        };
    }
}