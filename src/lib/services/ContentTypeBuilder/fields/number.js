module.exports = {
    getField(opt) {
        return {
            type_name: 'Number',
            name: opt.name,
            label: opt.label,
            default: opt.default || "",
            required: opt.required || null,
            private: opt.private || null,
            unique: opt.unique || null,
            index: opt.index || null,
            sparse: opt.sparse || null,
            //string specific options
            min: opt.min || null,
            man: opt.man || null,
         //   enum: opt.enum || null,
        };
    }
}