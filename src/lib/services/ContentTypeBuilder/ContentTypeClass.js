const {dropCollection} = require("../../db-helpers");
const {ContentTypeModel} = require('../../models/ContentTypeModel');
const {CustomModelsService} = require('./ContentModelService');
const Fields = require('./fields');
const debug = require('debug')('custom-type')

module.exports.CustomType = class {
    constructor(doc) {
        this.doc = doc;
    }

    async addFields(arg) {
        if (Array.isArray(arg)) {
            for (let key in arg) {
                const it = arg[key];
                let field = this.convertArgToField(it);
                this.doc.fields.push(field);
            }
        } else {
            let field = this.convertArgToField(arg);
            this.doc.fields.push(field);
        }
        await this.doc.save()
    }

    convertArgToField(arg) {
        if (!arg.field_type) {
            throw new Error('bad argument, field_type must be defined')
        }
        let func = Fields[arg.field_type].getField;
        if (!func || typeof func !== "function") {
            throw new Error(`bad argument, no field with type '${arg.field_type}' is defined`)
        }

        return func(arg);
    }

    async deleteFields(fieldName) {
        this.doc.fields = this.doc.fields.filter(it => it.name !== fieldName);
        await this.doc.save()
        this.doc
    }

    async delete() {
        let collectionName = this.doc.name;
        await this.doc.remove();

        const result = await dropCollection(collectionName);
        debug('delete', result);

        await CustomModelsService.registerAllModels()
    }

    getDocument() {
        return this.doc;
    }

    static async getInstance(collection_name) {
        const doc = await ContentTypeModel.findOne({name: collection_name})
        if (!doc) {
            throw new Error('bad argument, custom type not found!');
        }
        return new this(doc);
    }

    static async createNew(doc) {
        const ct = new ContentTypeModel(doc)
        await ct.save();
        return new this(ct);
    }
}