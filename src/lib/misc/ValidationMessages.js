module.exports.EVM = {
    empty:'should not be empty',
    text:'should be a text',
    number:'should be a number',
    boolean:'should be true or false',
    not_found:'Not Found',
    already_exists:'Already exists',
    error_500:'server side error',
}