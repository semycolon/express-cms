const mongoose = require('mongoose'),
    bcrypt = require('bcrypt'),
    SALTINESS = 10,
    jwt = require('jsonwebtoken'),
    JWT_SECRET = process.env.JWT_SECRET || 'not_secure_token',
    debug = require('debug')('userModel')

if (!process.env.JWT_SECRET) {
    debug('Error: process.env.JWT_SECRET is not available.')
}

const schema = new mongoose.Schema({
    username: {type: String, require: true, unique: true},
    password: {type: String, require: true,},
    superAdmin: {type: Boolean,},
}, {
    toJSON: {
        transform: function (doc, it) {
            delete it._id;
            delete it.__v;
            delete it.password;
        }
    }
});


schema.pre('save', function (next) {
    const user = this;
    if (!user.isModified('password')) next();

    bcrypt.genSalt(SALTINESS, function (err, salt) {
        if (err) next(err);
        bcrypt.hash(user.password, salt, function (err, hash) {
            if (err) next(err);
            user.password = hash;
            next();
        });
    });
});

schema.methods.comparePassword = function comparePassword(candidatePassword) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
            if (err) reject(err);
            resolve(isMatch);
        });
    });
};

schema.methods.generateJWT = function () {
    const user = this;
    return jwt.sign({username: user.username,}, JWT_SECRET);
};

schema.statics.verifyJwtToken = function (token) {
    const UserModel = this;
    return new Promise((resolve, reject) => {
        try {
            const {username} = jwt.verify(token, JWT_SECRET);
            UserModel.findOne({username}).then(user => {
                resolve(user)
            }, err => {
                reject(err);
            })
        } catch (e) {
            reject(e);
        }
    })
};

module.exports = mongoose.model('User', schema);