const mongoose = require('mongoose');
const {SchemaTypes} = mongoose;

const schema = new mongoose.Schema({
        name: {
            type: String,
            unique: true,
            required: true,
        },
        label: {
            type: String,
        },
        autoIndex: {
            type: SchemaTypes.Boolean,
            default: true,
        },
        timestamps: {
            type: SchemaTypes.Boolean,
            default: true,
        },
        fields: [{
            type:SchemaTypes.Mixed,
        }]
    }
);

module.exports.ContentTypeModel = mongoose.model('ContentType', schema);