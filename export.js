require('dotenv').config({path: '.env.development'});
const info = require('debug')('server:info');

const {generateApp} = require("./src/app");
const {initDatabase} = require("./src/lib/mongoose-connect");


module.exports.generateMiddleware = async function (apiBaseUrl = '/api', databaseConnectionString) {
    const successfulConnection = await initDatabase(databaseConnectionString);
    const app = await generateApp(apiBaseUrl)
    if (successfulConnection) {
        return app;
    }
    throw new Error('init database failed!,check you database connection');
}


