#Express CMS
---
Light weight , easy to use , headless cms for Express.js Applications 

##usage

```js
//add API to your express app
const ExportMiddleware = require('@semycolon/express-cms');
const cmsMiddleware = await ExportMiddleware.generateMiddleware('/cms-api/base-url')
app.use(cmsMiddleware);


//add react dashboard to your express app
const ReactDashboard = require('@semycolon/express-cms-react')
const reactMiddleware = ReactDashboard.generateMiddleware('/express-cms/dashboard', `http://domain.com/cms-api/base-url`);
app.use(reactMiddleware);
```

