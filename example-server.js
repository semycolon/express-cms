require('dotenv').config({path: '.env.development'});
const info = require('debug')('server:info');
const ExportMiddleware = require('./export');
const http = require('http');
const port = process.env.PORT || 8080;
const express = require('express');

(async () => {
    const app = express();
    const cmsMiddleware = await ExportMiddleware.generateMiddleware('/api')
    app.use(cmsMiddleware);
    const server = http.createServer(app);
    server.listen(port, listeningListener);
})()

function listeningListener() {
    info('Ta Daaaaaa ... server might be listening on http://localhost:' + port);
}

