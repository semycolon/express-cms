const mongoose = require('mongoose');

const userTemplate = {
    username: 'john',
    password: 'doedodoe',
}

module.exports = {
    async createTestUser() {
        const UserModel = mongoose.model('User');
        const user = new UserModel(userTemplate);
        await user.save()
        const token = await user.generateJWT();
        return token;
    }
}