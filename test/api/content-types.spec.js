const request = require('supertest')
const {generateApp} = require("../../src/app");
const express = require('express');
const app = express();
let ContentTypeModel
const mongoose = require('mongoose');
const {EVM} = require("../../src/lib/misc/ValidationMessages");
const {createTestUser} = require("./common");
const {initDatabase} = require("../../src/lib/mongoose-connect");
const cmsRouter = require('../../src/api/cms')

function checkValidationError(res) {
    expect(res.statusCode).toEqual(422)
    expect(res.body).toHaveProperty('errors')
}

let userToken;

function App() {
    const it = request(app);
    return it;
}

describe('API: Custom Type CRUD', () => {
    beforeAll(async function () {
        await initDatabase('mongodb://localhost:27017/test_cms');
        const contentTypeRouter = require('../../src/api/custom-types');
        app.use(contentTypeRouter);
        ContentTypeModel = mongoose.model('ContentType');
        await ContentTypeModel.deleteMany({})
        userToken = await createTestUser()

    });

    it('prepare: User model should be available', function () {
        expect(ContentTypeModel).toBeDefined();
        expect(ContentTypeModel.prototype instanceof mongoose.Model).toBe(true);
    });

    it('prepare: should empty collection', async function () {
        expect(await ContentTypeModel.find()).toHaveLength(0)
    });

    it('should return list of custom types', async function () {
        const {body} = await App()
            .get('/')
            .set('token', userToken)
            .send()
        expect(body).toBeDefined()
        expect(Array.isArray(body)).toBe(true)
    });

    const customTypeTemplate = {
        name: 'fooType',
        label: 'customBar'
    }

    it('should add new custom type', async function () {
        const {body} = await App()
            .post('/')
            .set('token', userToken)
            .send(customTypeTemplate)

        expect(body.find(it => it.name === customTypeTemplate.name)).toBeTruthy();

        const res = await App()
            .post('/')
            .set('token', userToken)
            .send(customTypeTemplate)

        checkValidationError(res)

    });

    const customField = {
        name: 'size',
        label: 'Size',
        field_type:'text',
    }

    it('should add new field', async function () {
        const res = await App()
            .post('/' + customTypeTemplate.name)
            .set('token', userToken)
            .send(customField)

        const {fields} = res.body;
        expect(
            fields.find(it => it.name = customField.name && it.label === customField.label)
        ).toBeTruthy()

        const res2 = await App()
            .post('/' + customTypeTemplate.name)
            .set('token', userToken)
            .send(customField)

        checkValidationError(res2);

        const {errors} = res2.body;
        expect(errors.find(it => it.param === 'name' && it.msg === EVM.already_exists)).toBeTruthy();
    });

    it('should throw validation errors', async function () {
        const res_bad_model_name = await App()
            .delete(`/not_${customTypeTemplate.name}/${customField.name}`)
            .set('token', userToken)
            .send();


        checkValidationError(res_bad_model_name);
        expect(
            res_bad_model_name.body.errors.find(it => it.param === 'name' && it.msg === EVM.not_found)
        ).toBeTruthy();

        const res_bad_field_name = await App()
            .delete(`/${customTypeTemplate.name}/not_${customField.name}`)
            .set('token', userToken)
            .send();

        checkValidationError(res_bad_model_name);
        expect(
            res_bad_field_name.body.errors.find(it => it.param === 'custom_field_name' && it.msg === EVM.not_found)
        ).toBeTruthy();
    });

    it('should delete field', async function () {
        const req = await App()
            .delete(`/${customTypeTemplate.name}/${customField.name}`)
            .set('token', userToken)
            .send();

        expect(req.body.fields.find(it => it.name === customField.name)).toBeFalsy()
    });

    it('should get detailed ', async function () {
        const res = await App()
            .get('/detailed/' + customTypeTemplate.name)
            .set('token', userToken)
            .send();
        expect(res.body.name).toBe(customTypeTemplate.name);
    });


    it('should insert data to collection', async function () {
        const res = await App()
            .post('/' + customTypeTemplate.name)
            .set('token', userToken)
            .send(customField)

        const app2 = express();
        app2.use(cmsRouter);

        const res2 = await request(app2)
            .post('/add/' + customTypeTemplate.name)
            .set('token', userToken)
            .send({
                size: 85,
            })
        expect(res2.statusCode).toBe(200);
        expect(Number(res2.body.size)).toBe(85);
    });

    it('should delete custom type and drop its collection', async function () {
        const res = await App()
            .delete('/' + customTypeTemplate.name)
            .set('token', userToken)
            .send()

        expect(res.body.find(it => it.name === customTypeTemplate.name)).toBeFalsy();
    });
})