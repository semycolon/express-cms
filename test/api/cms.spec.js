const request = require('supertest')
const {generateApp} = require("../../src/app");
const express = require('express');
const app = express();
let UserModel
const mongoose = require('mongoose');
const {initDatabase} = require("../../src/lib/mongoose-connect");

function checkValidationError(res) {
    expect(res.statusCode).toEqual(422)
    expect(res.body).toHaveProperty('errors')
}

describe('API: Auth', () => {
        beforeAll(async function () {
            await initDatabase('mongodb://localhost:27017/test_cms');
            const cmsRouter = require('../../src/api/cms');
            app.use(cmsRouter);

        });

    it('should do nothing', function () {

    });

    }
);
