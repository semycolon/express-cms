const request = require('supertest')
const {generateApp} = require("../../src/app");
const express = require('express');
const app = express();
let UserModel
const mongoose = require('mongoose');
const {initDatabase} = require("../../src/lib/mongoose-connect");

function checkValidationError(res) {
    expect(res.statusCode).toEqual(422)
    expect(res.body).toHaveProperty('errors')
}

describe('API: Auth', () => {
    beforeAll(async function () {
        await initDatabase('mongodb://localhost:27017/test_cms');
        const authRouter = require('../../src/api/auth');
        app.use(authRouter);
        UserModel = mongoose.model('User');
    });


    it('prepare: User model should be available', function () {
        expect(UserModel).toBeDefined();
        expect(UserModel.prototype instanceof mongoose.Model).toBe(true);
    });

    it('prepare: should empty collection', async function () {
        await UserModel.deleteMany({})
        expect(await UserModel.find()).toHaveLength(0)
    });

    it('register validation should work', async () => {
        const res = await request(app)
            .post('/register')
            .send()

        expect(res.statusCode).toEqual(422)
        expect(res.body).toHaveProperty('errors')
    })

    const userTemplate = {
        username: 'john',
        password: 'doedodoe',
        superAdmin:true,
    }

    it('should register new user', async () => {
        const res = await request(app)
            .post('/register')
            .send(userTemplate)

        expect(res.statusCode).toEqual(200)
        expect(res.body).toHaveProperty('token')
        expect(res.body).toHaveProperty('user')
        expect(res.body.user.username).toBe(userTemplate.username);
        expect(res.body).not.toHaveProperty('user.password')

    })

    it('should error duplicate username', async function () {
        const res = await request(app)
            .post('/register')
            .send(userTemplate)

        checkValidationError(res)
    });

    it('login validation should work', async function () {
        const res = await request(app)
            .post('/login')
            .send()

        checkValidationError(res);
    });

    it('login should work', async function () {
        const res = await request(app)
            .post('/login')
            .send(userTemplate)

        expect(res.statusCode).toEqual(200)
        expect(res.body).toHaveProperty('token')
        expect(res.body).toHaveProperty('user')
        expect(res.body.user.username).toBe(userTemplate.username);
        expect(res.body).not.toHaveProperty('user.password')
        token = res.body.token;
    });

    it('me should error with invalid token', async function () {
        const res = await request(app)
            .get('/me')
            .send()

        checkValidationError(res)
    });

    it('should authenticate by token', async function () {
        const token = (await request(app)
            .post('/login')
            .send(userTemplate)).body.token;

        const res = await request(app)
            .get('/me')
            .set('token', token)
            .send()

        expect(res.statusCode).toBe(200);
        expect(res.body.user.username).toBe(userTemplate.username);
    });


})