require('dotenv').config({path: '.env.development'})
const {initDatabase} = require("../../src/lib/mongoose-connect");
const mongoose = require('mongoose');
const {CustomType} = require("../../src/lib/services/ContentTypeBuilder/ContentTypeClass");
let CustomModelsService, ContentTypeModel;

describe('CRUD Custom Types', function () {
    beforeAll(async function () {
        await initDatabase('mongodb://localhost:27017/test_cms');
        CustomModelsService = require("../../src/lib/services/ContentTypeBuilder/ContentModelService").CustomModelsService;
        ContentTypeModel = mongoose.model('ContentType')

        await ContentTypeModel.deleteMany({})
    })

    let book_custom_type = {
        label: 'Book',
        name: 'book',
    };

    const fieldName = {
        field_type:'text',
        type_name: 'String',
        name: 'name',
        label: 'Name',
        default: 'no name',
        required: true,
        private: false,
        unique: true,
        min: 3,
        max: 45,
    }

    const fieldPrice = {
        field_type:'number',
        type_name: 'Number',
        name: 'price',
        label: 'Price',
        default: 0,
        required: true,
        private: false,
        unique: false,
        min: 0,
        max: 99999,
    }

    const fieldAuthor = {
        field_type:'text',
        type_name: 'String',
        name: 'author',
        label: 'Author',
        default: '',
        required: true,
        private: false,
        unique: false,
        min: 0,
        max: 99999,
    }

    it('create book custom type', async function () {
        const bookContentType = await CustomType.createNew(book_custom_type)

        expect(bookContentType.getDocument().name).toBeDefined()
        expect(bookContentType.getDocument().label).toBeDefined()
    });

    it('should add fields', async function () {
        const bookCustomType = await CustomType.getInstance(book_custom_type.name);
        await bookCustomType.addFields([fieldName, fieldPrice])

        let document = bookCustomType.getDocument();
        expect(document.fields[0].name).toEqual(fieldName.name)
        expect(document.fields[1].name).toEqual(fieldPrice.name)
    });

    it('should clear all documents of  ContentTypes', async function () {
        await CustomModelsService.registerAllModels();
        await CustomModelsService.emptyAllModels();
        let models = CustomModelsService.getAllModels();

        for (let k in models) {
            expect(await models[k].find({})).toHaveLength(0);
        }
    });

    it('should register custom collection', async function () {
        await CustomModelsService.registerAllModels()
        const BookModel = await CustomModelsService.getCustomModel(book_custom_type.name);
        let tmp = {name: 'animals farm', price: 20000};
        const book = new BookModel(tmp)
        await book.save();
        expect(book.name).toBe(tmp.name);
        expect(book.price).toBe(tmp.price);
    });

    it('should add another field to book model', async function () {
        const bookCustomType = await CustomType.getInstance(book_custom_type.name);
        await bookCustomType.addFields(fieldAuthor)

        await CustomModelsService.registerAllModels();

        const BookModel = await CustomModelsService.getCustomModel(book_custom_type.name);
        expect(BookModel.schema.path('author') instanceof mongoose.SchemaType).toBe(true);

        let author = 'john doe';
        const book = new BookModel({name: 'love story', price: 10000, author, foo: 'bar'})
        await book.save();
        expect(book.author).toBe(author);
    });

    it('should delete model book', async function () {
        const bookCustomType = await CustomType.getInstance(book_custom_type.name);
        await bookCustomType.delete()

        await expect(CustomType.getInstance(book_custom_type.name)).rejects.toThrow(/not found/);

        const collinfo = await new Promise((resolve, reject) => {
            mongoose.connection.db.listCollections({name: 'books'})
                .next(function (err, collinfo) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(collinfo);
                    }
                });
        })
        expect(collinfo).toBe(null);


    });


})
;