const mongoose = require('mongoose');

const {initDatabase} = require("../../../src/lib/mongoose-connect");
describe('User Model', function () {
    let UserModel;
    beforeAll(async function () {
        await initDatabase('mongodb://localhost:27017/test_cms');
        UserModel = mongoose.model('User');
    })

    it('userTemplate model should be available', function () {
        expect(UserModel).toBeDefined();
        expect(UserModel.prototype instanceof mongoose.Model).toBe(true);
    });

    it('should empty collection', async function () {
        await UserModel.deleteMany({})
        expect(await UserModel.find()).toHaveLength(0)
    });

    const userTemplate = {username: 'john', password: 'doedoe'}
    const userQuery = {username: userTemplate.username}

    it('should create new userTemplate', async function () {
        const user = new UserModel(userTemplate)
        await user.save();

        expect(user.username).toBeDefined();
        expect(user.username).toBe(userTemplate.username);

        expect(user.password).toBeDefined();
        expect(user.password).not.toBe(userTemplate.password);
    });

    it('should generate jwt token', async function () {
        const user = await UserModel.findOne(userQuery);
        expect(user.generateJWT).toBeDefined();
        const token = user.generateJWT();
        expect(token).toBeDefined();

        expect(UserModel.verifyJwtToken).toBeDefined();
        expect(await UserModel.verifyJwtToken(token)).toEqual(user);
    });
})